#include <iostream>
#include <vector>
using namespace std;

vector<int> & hamiltonian_paths(Graph const& g, int v, vector<bool> visited,
                       vector<int> &path, int N)
{
    if ((int) path.size() == N)
    {
        //for (int i : path) {cout << i << " ";}
        //cout << endl;
        return path;
    }

    for (int w : g.adj_list[v])
    {
        if (!visited[w])
        {
            visited[w] = true;
            path.push_back(w);

            //check if adding vertex w to the path leads
            //to a solution or not
            auto s = new vector<int>(hamiltonian_paths(g, w, visited, path, N));
            if (s->size() == N) {
                return *s;
            }

            //backtrack
            visited[w] = false;
            path.pop_back();
        }
    }

    auto *vec = new vector<int>;
    return *vec;
}

vector<int>& hamilton(int N){
    vector<int> path;
    path.push_back(0);
    vector<bool> visited(N);
    visited[0] = true;
    return hamiltonian_paths(Graph::RandomGraph(N), 0, visited, path, N);
}