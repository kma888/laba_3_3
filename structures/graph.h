#pragma once
#include "vector"
#include "iostream"
using namespace std;

struct Edge
{
    int src, dst;
};

class Graph
{

public:
    vector<vector<int>> adj_list{};
    int size = 0;

    Graph() = default;

    Graph(int N) {
        size = N;
        adj_list = vector<vector<int>>(N);
    }

    Graph(vector<Edge> const &edges, int N) : Graph(N)
    {
        //В N-ом векторе хранятся номера всех смежных с ней вершин (таблица смежности???)
        for (auto edge : edges) { add(edge); }
    }

    static Graph &RandomGraph(int N){
        auto *graph = new Graph(N);
        for (int i=0; i<N; i++){
            for (int j=i+1; j<N; j++){
                auto r = random()%2;
                if (r) {
                    graph->adj_list[i].push_back(j);
                    graph->adj_list[j].push_back(i);
                }
            }
        }
        return *graph;
    }

    void add(Edge &edge) {
        int src = edge.src;
        int dst = edge.dst;

        adj_list[src].push_back(dst);
        adj_list[dst].push_back(src);
    }

    void add() {
        auto *v = new vector<int>;
        adj_list.push_back(*v);
        size++;
    }

    void print() const{
        for (int i=0; i<size; i++){
            for (auto el: adj_list[i]){
                std::cout << el;
            }
            std::cout << '\n';
        }
    }
};
