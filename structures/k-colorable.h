#pragma once
#include <iostream>
#include <vector>

#include "graph.h"
using namespace std;



bool isValid(Graph const &graph, vector<int> color, int v, int c)
{
    for (int u : graph.adj_list[v])
        if (color[u] == c) {return false;}

    return true;
}

vector<int>& kColorable(Graph const &graph, vector<int>& color, int K, int v, int N)
{
    if (v == N)
    {
        //for (int i = 0; i < N; ++i)
            //cout << color[i] << " ";
        //cout << endl;
        return color;
    }

    for (int c = 1; c <= K; ++c)
    {
        if (isValid(graph, color, v, c))
        {
            color[v] = c;

            auto vec = new vector<int> (kColorable(graph, color, K, v+1, N));
            if (vec->size() == N){
                return *vec;
            }

            color[v] = 0;
        }
    }

    auto *q = new vector<int>;
    return *q;
}


vector<int>& coloring(long long N){
    vector<int> color(N, 0);
    int K = 4;
    return kColorable(Graph::RandomGraph(N), color, K, 0, N);
}