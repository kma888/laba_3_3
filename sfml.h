#include <SFML/Graphics.hpp>
#include "vector"


bool isSpriteHover(sf::FloatRect sprite, sf::Vector2f mp)
{
    if (sprite.contains(mp)){
        return true;
    }
    return false;
}

void draw_interface(){
    auto *graph = new Graph();
    int modeWidth = 1000;
    int modeHeight = 500;
    float size = 20.;
    sf::RenderWindow window(sf::VideoMode(modeWidth, modeHeight), "Draw figure");

    std::vector<sf::CircleShape> circles;
    std::vector<sf::Text> numbers, numbers2;
    std::vector<sf::ConvexShape> lines;
    int NumberOfVertexes = 1;

    sf::Font font;
    font.loadFromFile("/Users/mihail/Downloads/arial/arial.ttf");

    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        static sf::CircleShape* firstVertex = nullptr;
        static int fVN;

        if (event.type == sf::Event::MouseButtonPressed) {
            auto position = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
            bool exists = false;

            for (int i=0; i < circles.size(); i++) {
                if (isSpriteHover(circles[i].getGlobalBounds(), position)){
                    if (firstVertex) {
                        if (firstVertex==&circles[i]){
                            circles[i].setOutlineColor(sf::Color::Transparent);
                        }
                        else{
                            auto fVp = firstVertex->getPosition();
                            auto cp = circles[i].getPosition();

                            sf::ConvexShape convex;
                            convex.setPointCount(4);
                            convex.setPoint(0, sf::Vector2f(fVp.x + size - 1,fVp.y + size - 1));
                            convex.setPoint(1, sf::Vector2f(fVp.x + size - 1,fVp.y + size + 1));
                            convex.setPoint(3, sf::Vector2f(cp.x  + size + 1,cp.y  + size + 1));
                            convex.setPoint(2, sf::Vector2f(cp.x  + size - 1,cp.y  + size + 1));

                            lines.push_back(convex);
                            firstVertex->setOutlineColor(sf::Color::Transparent);

                            Edge *edge = new Edge{fVN, i};
                            graph->add(*edge);
                        }
                        firstVertex = nullptr;
                    }
                    else {
                        circles[i].setOutlineColor(sf::Color::Red);
                        firstVertex = &circles[i];
                        fVN = i;
                    }
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                if (firstVertex){
                    firstVertex->setOutlineColor(sf::Color::Transparent);
                    firstVertex = nullptr;
                } else {
                    auto circle = *new sf::CircleShape;
                    circle.setRadius(size);
                    circle.setPosition(position);
                    circle.setFillColor(sf::Color::White);
                    circle.setOutlineThickness(2.);
                    circles.push_back(circle);

                    sf::Text text(std::to_string(NumberOfVertexes++), font);
                    text.setCharacterSize(20);
                    text.setStyle(sf::Text::Regular);
                    text.setFillColor(sf::Color::Black);
                    text.setPosition(sf::Vector2f(position.x + size / 2, position.y));
                    numbers.push_back(text);

                    graph->add();
                }
            }

            sf::sleep(sf::milliseconds(500));
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
            int N = graph->size;
            vector<int> color(N, 0);
            int K = 2; // number of colors

            std::vector<int> coloring = kColorable(*graph, color, K, 0, N);

            while(coloring.empty()){
                K++;
                coloring = kColorable(*graph, color, K, 0, N);
            }

            vector<sf::Color> colors = {sf::Color::White, sf::Color::Red, sf::Color::Blue, sf::Color::Green,
                                        sf::Color::Magenta, sf::Color::Cyan, sf::Color::Yellow, sf::Color::Black};

            for (int i=0; i<N; i++){
                circles[i].setFillColor(colors[coloring[i]]);
            }

            sf::sleep(sf::milliseconds(500));
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter)) {
            if (!numbers2.empty()) {numbers2.clear();}

            vector<int> path;
            path.push_back(0);
            //mark start node as visited
            vector<bool> visited(graph->size);
            visited[0] = true;
            auto h_p = hamiltonian_paths(*graph, 0, visited, path, graph->size);
            int counter2 = 1;
            h_p.push_back(0);
            if (h_p.size() < graph->size) {
                sf::Text warning("No path found!", font);
                warning.setCharacterSize(20);
                warning.setStyle(sf::Text::Regular);
                warning.setFillColor(sf::Color::Red);
                warning.setPosition(sf::Vector2f{10,10});
                window.draw(warning);
                window.display();
                sf::sleep(sf::milliseconds(500));
            } else {
                for (int i = 0; i < graph->size; i++) {
                    int first = h_p[i];
                    int second = h_p[i + 1];

                    auto position = circles[first].getPosition();
                    auto position2 = circles[second].getPosition();
                    auto position3 = (position + position2) * float(0.5);

                    sf::Text text(std::to_string(counter2++), font);
                    text.setCharacterSize(20);
                    text.setStyle(sf::Text::Regular);
                    text.setFillColor(sf::Color::Cyan);
                    text.setPosition(position3);
                    numbers2.push_back(text);
                }
            }

            sf::sleep(sf::milliseconds(500));
        }

        window.clear();
        for (auto const& s: circles) { window.draw(s); }
        for (auto const& s: numbers) { window.draw(s); }
        for (auto const& s: numbers2) { window.draw(s); }
        for (auto const& s: lines)   { window.draw(s); }
        window.display();
    }

    window.close();
}


void random_graph(int N){
    assert(N>0);
    auto graph = Graph::RandomGraph(N);
    graph.print();

    int modeWidth = 1000;
    int modeHeight = 500;
    float size = 20.;
    sf::RenderWindow window(sf::VideoMode(modeWidth, modeHeight), "Random graph");

    std::vector<sf::CircleShape> circles;
    std::vector<sf::Text> numbers, numbers2;
    std::vector<sf::ConvexShape> lines;
    int counter = 1;

    sf::Font font;
    font.loadFromFile("/Users/mihail/Downloads/arial/arial.ttf");


    float radius = 200.;
    float angle = 2 * M_PI / N;
    sf::Vector2f center{500, 250};

    for (int i=0; i<N; i++){
        auto circle = *new sf::CircleShape;
        auto position = center+sf::Vector2f{cos(angle*i),sin(angle*i)}*radius;
        circle.setRadius(size);
        circle.setPosition(position);
        circle.setFillColor(sf::Color::White);
        circle.setOutlineThickness(2.);
        circles.push_back(circle);

        sf::Text text(std::to_string(counter++), font);
        text.setCharacterSize(20);
        text.setStyle(sf::Text::Regular);
        text.setFillColor(sf::Color::Black);
        text.setPosition(sf::Vector2f(position.x + size / 2, position.y));
        numbers.push_back(text);
    }

    for (int i=0; i<N; i++){
        //std::cout << '\n';
        auto cp1 = circles[i].getPosition();
        for (int j=0; j<graph.adj_list[i].size(); j++){
            int A = graph.adj_list[i][j];
            //std::cout << A;
            auto cp2 = circles[A].getPosition();
            sf::ConvexShape convex;
            convex.setPointCount(4);
            convex.setPoint(0, sf::Vector2f(cp1.x + size - 1,cp1.y + size - 1));
            convex.setPoint(1, sf::Vector2f(cp1.x + size - 1,cp1.y + size + 1));
            convex.setPoint(3, sf::Vector2f(cp2.x + size + 1,cp2.y + size + 1));
            convex.setPoint(2, sf::Vector2f(cp2.x + size - 1,cp2.y + size + 1));
            lines.push_back(convex);
        }
    }

    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)){
            int N = graph.size;
            vector<int> color(N, 0);
            int K = 2; // number of colors


            std::vector<int> coloring = kColorable(graph, color, K, 0, N);

            //for (auto &c: coloring) {
            //    std::cout << c << "\t";
            //}

            while(coloring.empty()) {
                K++;
                coloring = kColorable(graph, color, K, 0, N);
            };

            vector<sf::Color> colors = {sf::Color::White, sf::Color::Red, sf::Color::Blue, sf::Color::Green,
                                        sf::Color::Magenta, sf::Color::Cyan, sf::Color::Yellow, sf::Color::Black};

            for (int i=0; i<N; i++){
                circles[i].setFillColor(colors[coloring[i]]);
            }

            sf::sleep(sf::milliseconds(500));
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter)) {
            if (!numbers2.empty()) {numbers2.clear();}
            vector<int> path;
            path.push_back(0);
            vector<bool> visited(N);
            visited[0] = true;
            auto h_p = hamiltonian_paths(graph, 0, visited, path, N);
            int counter2 = 1;
            h_p.push_back(0);

            if (h_p.size() < N) {
                sf::Text warning("No path found!", font);
                warning.setCharacterSize(20);
                warning.setStyle(sf::Text::Regular);
                warning.setFillColor(sf::Color::Red);
                warning.setPosition(sf::Vector2f{10,10});
                window.draw(warning);
                window.display();
                sf::sleep(sf::milliseconds(500));
            } else {
                for (int i = 0; i < N; i++) {
                    int first = h_p[i];
                    int second = h_p[i + 1];

                    auto position = circles[first].getPosition();
                    auto position2 = circles[second].getPosition();
                    auto position3 = (position + position2) * float(0.5);

                    sf::Text text(std::to_string(counter2++), font);
                    text.setCharacterSize(20);
                    text.setStyle(sf::Text::Regular);
                    text.setFillColor(sf::Color::Cyan);
                    text.setPosition(position3);
                    numbers2.push_back(text);
                }
            }

            sf::sleep(sf::milliseconds(500));
        }

        window.clear();
        for (auto const& s: circles) { window.draw(s); }
        for (auto const& s: numbers) { window.draw(s); }
        for (auto const& s: numbers2) { window.draw(s); }
        for (auto const& s: lines)   { window.draw(s); }
        window.display();
    }
    window.close();
}